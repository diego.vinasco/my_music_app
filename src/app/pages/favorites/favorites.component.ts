import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.sass']
})
export class FavoritesComponent implements OnInit {
  songs !: any;

  constructor(
    private spotify : SpotifyService,
    private router : Router
  ) { }

  ngOnInit(): void {
    this.spotify.isLogged.subscribe((res)=>{
      if(!res){
        this.router.navigate(['login']);
      }
    });
    
    this.spotify.getFavoriteSongs()
      .pipe()
      .subscribe(song => {this.songs = song.items});

  }
}
