import { Component,OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SpotifyService } from 'src/app/services/spotify.service';
 
@Component({
  selector:'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})


export class loginComponent implements OnInit {
  

  constructor(
    private spotify: SpotifyService,
    private router: Router
  ) { }

  private URLToken!: string;
  authURL : string = this.spotify.authURL;

  ngOnInit(): void {
    this.spotify.isLogged.subscribe((res)=>{
      if(res){
        this.router.navigate(['']);
      }
    });
    this.URLToken = window.location.hash.substring(1);
    window.location.hash = '';
    if(this.URLToken){
      this.spotify.login(this.URLToken)
    }
  }
    
}