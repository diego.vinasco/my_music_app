import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  songs !: any;

  constructor(
    private spotify : SpotifyService,
    private router : Router
  ) { }

  ngOnInit(): void {
    this.spotify.isLogged.subscribe((res)=>{
      if(!res){
        this.router.navigate(['login']);
      }
    });
    
    this.spotify.getLastReleases()
      .pipe()
      .subscribe(song => {this.songs = song.albums.items; console.log(song)});

  }


}
