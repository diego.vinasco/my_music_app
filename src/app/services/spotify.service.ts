import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
  private loggedIn = new BehaviorSubject<boolean>(false);

  public credentials = {
    clientId: environment.SPOTIFY_CLIENT_ID,
    clientSecret : environment.SPOTIFY_CLIENT_SECRET,
    scopes : ['user-library-modify','user-library-read']
  }

  public authURL = 'https://accounts.spotify.com/es-ES/authorize?client_id='+
    this.credentials.clientId + '&response_type=token'+
    '&redirect_uri=' + encodeURIComponent('http://localhost:4200/callback')+
    '&scope='+ this.credentials.scopes.join("%20") +
    '&expires_in=3600';

  constructor(private http: HttpClient) {
    this.checkToken();
  }

  get isLogged(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  login(token:string): void {
    this.saveToken(token);
    this.loggedIn.next(true);
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('type');
    localStorage.removeItem('expires');
    this.loggedIn.next(false);
  }

  private checkToken(): void { 
    const userToken = localStorage.getItem('token');

    //==PENDING======
    var isExpired : boolean = true;
    if(userToken !== null){
      isExpired = false;
    }
    //===============
    isExpired ? this.logout() : this.loggedIn.next(true);
  }

  private saveToken(token: string) {
    const aux = token.split('&').map(val=> {return val.split('=')});
    localStorage.setItem('token',aux[0][1]);
    localStorage.setItem('type',aux[1][1]);
    localStorage.setItem('expires',aux[2][1]);
  }

  getLastReleases(): Observable<any> {

    const headers = new HttpHeaders({
      'Authorization': localStorage.getItem('type')+' '+localStorage.getItem('token')
    })

    return this.http.get('https://api.spotify.com/v1/browse/new-releases',{ headers });
  }

  getFavoriteSongs(): Observable<any> {
    
    const headers = new HttpHeaders({
      'Authorization': localStorage.getItem('type')+' '+localStorage.getItem('token')
    })

    return this.http.get('https://api.spotify.com/v1/me/tracks',{ headers });
  }

  getUserData(): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': localStorage.getItem('type')+' '+localStorage.getItem('token')
    })

    return this.http.get('https://api.spotify.com/v1/me',{ headers });
  }
}