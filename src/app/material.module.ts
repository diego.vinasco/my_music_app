import { NgModule } from "@angular/core";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIcon, MatIconModule } from '@angular/material/icon';

@NgModule({
    exports: [MatToolbarModule,MatButtonModule,MatCardModule,MatIconModule]
})

export class MaterialModule { }