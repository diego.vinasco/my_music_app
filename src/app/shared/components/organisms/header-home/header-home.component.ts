import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-header-home',
  templateUrl: './header-home.component.html',
  styleUrls: ['./header-home.component.sass']
})
export class HeaderHomeComponent implements OnInit {
  public userName : string = '';

  constructor(
    private spotify: SpotifyService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.spotify.getUserData()
    .pipe()
    .subscribe(data => this.userName = data.display_name);
  }

  onLogout(): void {
    this.spotify.logout();
  }

  goHome(): void {
    this.router.navigate(['']);
  }

  goFavorites(): void {
    this.router.navigate(['favorites']);
  }

}
