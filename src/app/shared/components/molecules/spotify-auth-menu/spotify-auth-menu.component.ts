import { Component, Input ,OnInit } from '@angular/core';

@Component({
  selector: 'app-spotify-auth-menu',
  templateUrl: './spotify-auth-menu.component.html',
  styleUrls: ['./spotify-auth-menu.component.sass']
})
export class SpotifyAuthMenuComponent implements OnInit {

  @Input() authURL!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
