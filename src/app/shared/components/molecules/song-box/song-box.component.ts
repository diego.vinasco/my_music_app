import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-song-box',
  templateUrl: './song-box.component.html',
  styleUrls: ['./song-box.component.sass']
})
export class SongBoxComponent implements OnInit {
  @Input() song : any;

  constructor() { }

  ngOnInit(): void {
  }

}
