import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-spotifyAuthButton',
  templateUrl: './spotifyAuthButton.component.html',
  styleUrls: ['./spotifyAuthButton.component.sass']
})
export class spotifyAuthButtonComponent implements OnInit {

  @Input() authURL!: string;
    
  constructor() { }

  ngOnInit(): void { }

  onClick(): void {
    window.location.href = this.authURL;
  }

}
