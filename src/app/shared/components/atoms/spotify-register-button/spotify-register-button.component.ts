import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spotify-register-button',
  templateUrl: './spotify-register-button.component.html',
  styleUrls: ['./spotify-register-button.component.sass']
})
export class SpotifyRegisterButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClick(): void {
    window.location.href = 'https://www.spotify.com/co/signup';
  }

}
