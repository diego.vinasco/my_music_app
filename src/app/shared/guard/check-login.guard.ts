import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable,take,map } from 'rxjs';
import { SpotifyService } from 'src/app/services/spotify.service';

@Injectable({
  providedIn: 'root'
})
export class CheckLoginGuard implements CanActivate {

  constructor(
    private spotify : SpotifyService
  ) {}

  canActivate(): Observable<boolean> {
    return this.spotify.isLogged.pipe(
      take(1),
      map( (isLogged : boolean) => {console.log(!isLogged); return !isLogged})
    );
  }
  
}
