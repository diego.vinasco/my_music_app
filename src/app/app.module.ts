import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { loginComponent } from './pages/login/login.component';
import { spotifyAuthButtonComponent } from './shared/components/atoms/spotifyAuthButton/spotifyAuthButton.component';
import { SpotifyRegisterButtonComponent } from './shared/components/atoms/spotify-register-button/spotify-register-button.component';
import { SpotifyAuthMenuComponent } from './shared/components/molecules/spotify-auth-menu/spotify-auth-menu.component';
import { HomeComponent } from './pages/home/home.component';
import { SongBoxComponent } from './shared/components/molecules/song-box/song-box.component';
import { HeaderLoginComponent } from './shared/components/organisms/header-login/header-login.component';
import { HeaderHomeComponent } from './shared/components/organisms/header-home/header-home.component';
import { FooterComponent } from './shared/components/organisms/footer/footer.component';
import { FavoritesComponent } from './pages/favorites/favorites.component';

@NgModule({
  declarations: [
    AppComponent,
    loginComponent,
    spotifyAuthButtonComponent,
    SpotifyRegisterButtonComponent,
    SpotifyAuthMenuComponent,
    HomeComponent,
    SongBoxComponent,
    HeaderLoginComponent,
    HeaderHomeComponent,
    FooterComponent,
    FavoritesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
